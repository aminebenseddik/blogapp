import { Router } from 'express';
import postRoutes from './post.routes';

const router = Router();

// Mount post routes at /posts
router.use('/articles', postRoutes);

export default router;
