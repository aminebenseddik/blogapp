import mongoose from 'mongoose';

/**
 * Comment Schema
 */
const CommentSchema = new mongoose.Schema({
  body: String,
  author: String,
  updated: {
    type: Date,
    default: Date.now,
  },
  likes: {
    type: Number,
    default: 0,
  },
  post: {
    ref: 'Post',
    type: mongoose.Schema.Types.ObjectId,
  },
});

export default mongoose.model('Comment', CommentSchema);
