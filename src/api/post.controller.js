import Post from './post.model';
import Comment from './comment.model';

/**
 * Load post and attach it to request object
 */
const load = (req, res, next, id) => {
  /** Get current post */
  Post.get(id)
    .then((post) => {
      /** Add post object to request */
      req.post = post; // eslint-disable-line no-param-reassign
      return next();
    })
    .catch(e => next(e));
};

/**
 * Get a post by his :postId
 * route GET /api/posts/:postId
 */
const get = (req, res, next) => {
  /** Use mongoose populate method to get all comments */
  req.post.populate('comments', (err, post) => {
    if (err) { return next(err); }

    /** Return the post and its comments */
    return res.json(post);
  });
};

/**
 * List all posts
 */
const list = (req, res, next) => {
  /** Get skip and limit param if supplied in query */
  let { skip = 0, limit = 50 } = req.query;

  /** parseInt from params */
  skip = parseInt(skip, 10);
  limit = parseInt(limit, 10);

  /** Use Post model list method */
  Post.list({ skip, limit })
    .then(posts => res.json(posts))
    .catch(e => next(e));
};

/**
 * Create new Post
 */
const create = (req, res, next) => {
  /** Get request body */
  const { body } = req;

  /** Create new Post using supplied request body */
  const post = new Post(body);

  /** Save Post using mongoose save method */
  post.save()
    .then(savedPost => res.json(savedPost))
    .catch(e => next(e));
};

/**
 * Add like
 */
const like = (req, res, next) => {
  /** Get current post */
  const { post } = req;

  /** Increment likes */
  post.likes += 1;

  /** Save Post using mongoose save method */
  post.save()
    .then(savedPost => res.json(savedPost))
    .catch(e => next(e));
};

/**
 * Add comment to post
 * route POST /api/posts/:postId/comments
 */
const comment = (req, res, next) => {
  /** Get current post and request body */
  const { post, body } = req;

  /** Create new Comment from request body */
  const com = new Comment(body);

  /** Add postId to the new comment */
  com.post = post;

  /** Save comment */
  com.save()
    .then((savedComment) => {
      /** Push commentId to the post */
      post.comments.push(savedComment);

      /** Update post */
      post.save()
        .then()
        .catch(e => next(e));

      /** Return saved comment */
      return res.json(savedComment);
    })
    .catch(e => next(e));
};

export default { list, create, get, load, like, comment };
