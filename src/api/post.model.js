import mongoose from 'mongoose';
import HTTPStatus from 'http-status';
import AppError from '../helpers/apperror';

/**
 * Post Schema
 */
const PostSchema = new mongoose.Schema({
  title: String,
  subtitle: String,
  text: String,
  link: String,
  author: String,
  updated: {
    type: Date,
    default: Date.now,
  },
  likes: {
    type: Number,
    default: 0,
  },
  comments: [{
    ref: 'Comment',
    type: mongoose.Schema.Types.ObjectId,
  }],
});

/**
 * Post Schema static methods
 * http://mongoosejs.com/docs/guide.html
 */
PostSchema.statics = {
  /**
   * Get a Post by its ObjectId
   * @param {ObjectId} id - The ObjectId of the post
   * @returns {Promise} Post
   */
  get(id) {
    // Check if supplied id is valid
    // Argument passed in must be a single String of 12 bytes or a string of 24 hex characters
    if (!id.match(/^[0-9a-fA-F]{24}$/)) {
      throw new AppError('Not a valid postID', HTTPStatus.BAD_REQUEST);
    }

    return this.findById(id)
      .exec()
      .then((post) => {
        if (post) {
          return post;
        }
        const err = new AppError('Post not found', HTTPStatus.NOT_FOUND);
        return Promise.reject(err);
      });
  },

  /**
   * List all posts in descending order of 'updated'
   * Returns a list of the 50 posts by default
   * @param {Object} params - query parameters
   * @returns {Promise} Post[]
   */
  list(params = {}) {
    const { skip = 0, limit = 50 } = params;
    return this.find()
      .sort({ updated: 'desc' })
      .skip(skip)
      .limit(limit)
      .exec();
  },
};

export default mongoose.model('Post', PostSchema);
