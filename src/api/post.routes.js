import { Router } from 'express';
import validate from 'express-validation';

import paramValidation from '../helpers/param-validation';
import postCtrl from './post.controller';

const router = Router();

router.route('/')
  /**
   * GET /posts - Get all of the posts
   */
  .get(postCtrl.list)
  /**
   * POST /posts - Create new post
   */
  .post(validate(paramValidation.createPost), postCtrl.create);

router.route('/:postId')
  /**
   * GET /posts/:postId - Get a post
   */
  .get(postCtrl.get);

router.route('/:postId/like')
  /**
   * PUT /posts/:postId/like - Add a like to a post
   */
  .put(postCtrl.like);

router.route('/:postId/comments')
  /**
   * POST /post/:postId/comments - Add new comment
   */
  .post(validate(paramValidation.createComment), postCtrl.comment);

// Load Post when route with postId is hit
router.param('postId', postCtrl.load);

export default router;
