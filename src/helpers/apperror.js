import HTTPStatus from 'http-status';

/**
 * @extends Error
 */
class ExtendableError extends Error {
  constructor(message, status) {
    super(message);
    this.name = this.constructor.name;
    this.message = message;
    this.status = status;
    Error.captureStackTrace(this, this.constructor.name);
  }
}

/**
 * Application custom Error class
 * @extends ExtendableError
 */
class AppError extends ExtendableError {
  constructor(message, status = HTTPStatus.INTERNAL_SERVER_ERROR) {
    super(message, status);
  }
}

export default AppError;
