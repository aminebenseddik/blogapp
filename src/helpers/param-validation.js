import Joi from 'joi';

export default {
  createPost: {
    body: {
      title: Joi.string().required(),
      link: Joi.string().uri().required(),
      likes: Joi.number(),
    },
  },
  createComment: {
    body: {
      author: Joi.string().required(),
      body: Joi.string().uri().required(),
      likes: Joi.number(),
    },
  },
};
