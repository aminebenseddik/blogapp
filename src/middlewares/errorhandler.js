import HTTPStatus from 'http-status';
import AppError from '../helpers/apperror';

/**
 * [notFoundHandler Handles 404 Error and forward to (next) errorHandler middleware]
 *
 * In Express, 404 responses are not the result of an error.
 * Express has executed all middleware functions and routes, and found that none of them responded
 *
 * @param  {[Object]}   req  [Request Object]
 * @param  {[Object]}   res  [Response Object]
 * @param  {Function} next [Send to next middleware]
 */
const notFoundHandler = (req, res, next) => {
  const err = new AppError('Not Found', HTTPStatus.NOT_FOUND);
  return next(err);
};

/**
 * [errorHandler Handles Errors]
 *
 * Catch Errors and renders error.ejs view.
 * In production mode no stack trace is shown to the users
 *
 * @param  {Object} err [Error Object]
 * @param  {Object} req [Request Object]
 * @param  {Object} res [Response Object]
 */
const errorHandler = (err, req, res, next) => { // eslint-disable-line no-unused-vars
  res.status(err.status || HTTPStatus.INTERNAL_SERVER_ERROR);
  const error = req.app.get('env') === 'development' ? err : {};

  res.json({
    message: err.message,
    status: error.status,
    stack: error.stack,
  });
};

export { errorHandler, notFoundHandler };
