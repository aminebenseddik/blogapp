import Joi from 'joi';
import HTTPStatus from 'http-status';
import AppError from '../helpers/apperror';

const validate = (schema) => {
  if (!schema) throw new Error('No Joi Schema provided');

  return (
    (req, res, next) => {
      const result = Joi.validate(req.body, schema.body);

      if (result.error) {
        return next(new AppError('Validation Error', HTTPStatus.BAD_REQUEST));
      }
      return next();
    }
  );
};

export default validate;
