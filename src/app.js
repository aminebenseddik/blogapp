import http from 'http';
import express from 'express';
import logger from 'morgan';
import cors from 'cors';
import compress from 'compression';
import helmet from 'helmet';
import cookieParser from 'cookie-parser';
import bodyParser from 'body-parser';
import mongoose from 'mongoose';
import config from './config.json';

import {
  errorHandler,
  notFoundHandler,
} from './middlewares/errorhandler';

import api from './api';

const app = express();
app.server = http.createServer(app);

/**
 * Connect to MongoDB
 */
mongoose.Promise = global.Promise;
mongoose.connect(config.db);
mongoose.connection.on('error', () => {
  throw new Error('DB Connection Error');
});

/**
 * Morgan logger
 */
app.use(logger('dev'));

/**
 * 3rd Party Middleware
 */
app.use(bodyParser.json({ limit: config.bodyLimit }));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(compress());

/**
 * Secure app by setting various HTTP headers
 */
app.use(helmet());

/**
 * Enable CORS - Cross Origin Resource Sharing
 */
app.use(cors());

/**
 * API Routes on '/api'
 * Exemple: GET /api/posts
 */
app.use('/api/v1', api);

/**
 * Add Error Handler Middlewares
 */
app.use(notFoundHandler);
app.use(errorHandler);

/**
 * Start the App
 */
app.server.listen(process.env.PORT || config.port);

export default app;
