// curl -vX POST http://localhost:4000/api/v1/articles -d @seed-data.json --header "Content-Type: application/json"
import http from 'http';
import loremIpsum from 'lorem-ipsum'; // eslint-disable-line


const text = loremIpsum({
  count: 3,                  // Number of words, sentences, or paragraphs to generate.
  units: 'paragraphs',        // Generate words, sentences, or paragraphs.
  format: 'html',            // Plain text or html
});

const title = loremIpsum({
  count: 1,                  // Number of words, sentences, or paragraphs to generate.
  units: 'sentences',        // Generate words, sentences, or paragraphs.
  format: 'plain',            // Plain text or html
});

const subtitle = loremIpsum({
  count: 1,                  // Number of words, sentences, or paragraphs to generate.
  units: 'sentences',        // Generate words, sentences, or paragraphs.
  format: 'plain',            // Plain text or html
});

const author = loremIpsum({
  count: 1,
  units: 'words',
  format: 'plain',
  words: ['Françoise', 'Amine', 'Clement', 'Rafik', 'François'],
});

const data = {
  title,
  subtitle,
  text,
  author,
  link: 'https://unsplash.it/1900x600',
};

const options = {
  hostname: 'localhost',
  port: 4000,
  path: '/api/v1/articles',
  method: 'POST',
  headers: {
    'Content-Type': 'application/json',
  },
};

const req = http.request(options, (res) => {
  console.log(`Status: ${res.statusCode}`);
  console.log(`Headers: ${JSON.stringify(res.headers)}`);

  res.setEncoding('utf8');
  res.on('data', (body) => {
    console.log(`Body: ${body}`);
  });
});

req.on('error', (e) => {
  console.log(`problem with request: ${e.message}`);
});


// write data to request body
req.write(JSON.stringify(data));
req.end();
