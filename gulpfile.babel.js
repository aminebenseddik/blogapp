import gulp from 'gulp';
import browserSync from 'browser-sync';
import nodemon from 'gulp-nodemon';
import config from './src/config.json';

gulp.task('browser-sync', ['nodemon'], () => {
  browserSync.init(null, {
    proxy: `http://localhost:${config.port}`,
    files: ['src/public/**/*.*', 'src/views/**/*.*'],
    open: false,
    port: 7000,
  });
});

gulp.task('nodemon', (cb) => {
  let started = false;

  return nodemon({
    script: 'src/app.js',
    ext: 'js',
    watch: ['src'],
    env: {
      NODE_ENV: 'development',
    },
    exec: 'babel-node',
  }).on('start', () => {
    if (!started) {
      cb();
      started = true;
    }
  }).on('restart', () => {
    setTimeout(() => {
      browserSync.reload();
    }, 1500);
  });
});

gulp.task('default', ['browser-sync'], () => {});
